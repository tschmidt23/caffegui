//#define CPU_ONLY

#include <pangolin/pangolin.h>
#include <GL/freeglut.h>
#include <iostream>
#include <Eigen/Dense>
#include <caffe/caffe.hpp>
#include <caffe/common.hpp>
#include <vector_types.h>
#include <endian.h>

const int guiWidth = 1280;
const int guiHeight = 960;
const int panelWidth = 180;

inline float4 glColorHSV( GLfloat hue, GLfloat s=1.0f, GLfloat v=1.0f )
{
    const GLfloat h = 360.0f*hue / 60.0f;
    const int i = (int)floor(h);
    const GLfloat f = (i%2 == 0) ? 1-(h-i) : h-i;
    const GLfloat m = v * (1-s);
    const GLfloat n = v * (1-s*f);
    switch(i)
    {
    case 0: return make_float4(v,n,m,1);
    case 1: return make_float4(n,v,m,1);
    case 2: return make_float4(m,v,n,1);
    case 3: return make_float4(m,n,v,1);
    case 4: return make_float4(n,m,v,1);
    case 5: return make_float4(v,m,n,1);
    default:
        return make_float4(0,0,0,1);
    }
}

const std::string mnistTrainImageFile = "data/mnist/train-images-idx3-ubyte";
const std::string mnistTrainLabelFile = "data/mnist/train-labels-idx1-ubyte";
const std::string mnistTestImageFile = "data/mnist/t10k-images-idx3-ubyte";
const std::string mnistTestLabelFile = "data/mnist/t10k-labels-idx1-ubyte";
const int mnistMagicLabelNumber = 2049;
const int mnistMagicImageNumber = 2051;

void loadMNISTLabels(const std::string filename, std::vector<unsigned char>& labels);
float* loadMNISTImages(const std::string filename);

void printBlobInfo(boost::shared_ptr<caffe::Net<float> > & testNet,  std::string blobname ){

    boost::shared_ptr<caffe::Blob<float> > featABlob = testNet->blob_by_name(blobname);

    const int channels= featABlob->channels();
    const int batchsize= featABlob->num();
    const int num_axes= featABlob->num_axes();
    const int width= featABlob->width();
    const int height= featABlob->height();

    //e,g,
    //channels 20.
    //batchsize 200.
    //count 2304000.
    //num_axes 4. //
    //width 24. (this is computed by taking this blobs input layer and subtracting 1/2 the kernel size)
    //height 24. //see above.
    const int count= featABlob->count();  //=width*height*batchsize*channels,. i.e. number of floats in this blob.


    std::cout << " ===================================== " << std::endl;

    std::cout << "INFO::" << blobname << std::endl;
    std::cout << "channels " << channels << "." << std::endl;
    std::cout << "batchsize " << batchsize << "." << std::endl;
    std::cout << "count " << count << "." << std::endl;
    std::cout << "num_axes " << num_axes << "." << std::endl;
    std::cout << "width " << width << "." << std::endl;
    std::cout << "height " << height << "." << std::endl;
    std::cout << " ------------------------------------- \n\n" << std::endl;



}

/*
A x =y
B y = z

d BAx /  B and A.

*/

int main(int argc, char** argv) {

    FLAGS_alsologtostderr = 1;

    //    if (argc < 2) { std::cerr << "usage: caffeGui solver.prototxt" << std::endl; return 1; }

    caffe::GlobalInit(&argc,&argv);

    caffe::SolverParameter solverParam;
    caffe::ReadProtoFromTextFileOrDie("examples/siamese/mnist_siamese_solver.prototxt", &solverParam);
    //    caffe::ReadProtoFromTextFileOrDie(std::string(argv[1]), &solverParam);

    std::vector<unsigned char> testLabels;
    float numSemanticClasses = 10;
    const bool loadTest = true;
    float* testData ;
    if(loadTest){
        loadMNISTLabels(mnistTestLabelFile,testLabels);
        testData = loadMNISTImages(mnistTestImageFile);
    }else{
        loadMNISTLabels(mnistTrainLabelFile,testLabels);
        testData = loadMNISTImages(mnistTrainImageFile);
    }

    caffe::Caffe::SetDevice(0);
    caffe::Caffe::set_mode(caffe::Caffe::GPU);

    boost::shared_ptr<caffe::Solver<float> > solver(caffe::GetSolver<float>(solverParam));


    //    solver->Solve();

    //load the test
    boost::shared_ptr<caffe::Net<float> > testNet = solver->test_nets()[0];// solver->net(); // there is only one training net.
    //boost::shared_ptr<caffe::Net<float> > testNet = solver->test_nets()[0]; //possibly multiple test nets

    boost::shared_ptr<caffe::Blob<float> > featABlob = testNet->blob_by_name("feat");
    boost::shared_ptr<caffe::Blob<float> > featBBlob = testNet->blob_by_name("feat_p");
    boost::shared_ptr<caffe::Blob<float> > pairBlob = testNet->blob_by_name("pair_data");

    boost::shared_ptr<caffe::Layer<float> > loss = testNet->layer_by_name("loss");

    //this is the batch size for the network --
    //note test net will have batch size that is set
    //to ensure that the complete testing data size is covered.
    //for example a testing sample size of N, will require
    //some batchsize = N/testIterations to ensure the total
    //error is measured for all N in batches of batchsize.

    //each blob has the same batch size (number of test or training samples.
    int num = featABlob->num();

    for(std::string n: testNet->blob_names()){
        printBlobInfo(testNet,n);
    }


    int nD_output = featABlob->channels();
    std::vector<float3> mappedPoints;


    int width_input_img = pairBlob->width();
    int height_input_img = pairBlob->height();

    pangolin::CreateGlutWindowAndBind("Main",guiWidth+panelWidth,guiHeight,GLUT_MULTISAMPLE | GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

    glewInit();

    pangolin::OpenGlRenderState camState;//(pangolin::ProjectionMatrix(640,480,420,420,320,240,0.1,500));
    pangolin::OpenGlMatrixSpec glK = pangolin::ProjectionMatrixRDF_TopLeft(640,480,500,500,320,240,0.01,1000);
    camState.SetProjectionMatrix(glK);

    pangolin::OpenGlMatrixSpec id = pangolin::IdentityMatrix(pangolin::GlModelViewStack);
    //id.m[0]=-1;
    //const TooN::SE3<> identity;
    camState.SetModelViewMatrix(id);

    pangolin::View& imgDisp = pangolin::Display("camera").SetAspect(640.0f/480.0f).SetHandler(new pangolin::Handler3D(camState,pangolin::AxisNone,0.01,0.5));

    //pangolin::View& imgDisp = pangolin::Display("img").SetAspect(640.0f/480.0f);

    pangolin::CreatePanel("panel").SetBounds(0.0, 1.0, 0.0, pangolin::Attach::Pix(panelWidth));

    pangolin::Display("display")
            .SetBounds(1.0,0.0,1.0,pangolin::Attach::Pix(panelWidth))
            .SetLayout(pangolin::LayoutEqual)
            //            .AddDisplay(camDisp)
            .AddDisplay(imgDisp);

    pangolin::Var<bool> step("panel.step",false,true);
    pangolin::Var<int> stepIters("panel.stepIters",1,1,1000);
       pangolin::Var<float> base_lr("panel.base_lr",0.001,0.0001, 0.1);
       pangolin::Var<float> moment("panel.moment",0.9,0.0001, 1);
       pangolin::Var<float> margin("panel.margin",0.9,0.0001, 1);


    //pangolin::Var<bool> reset("panel.reset",false,false);

    //   pangolin::Var<float> minX("panel.minX",-5,-10, -1);
    //   pangolin::Var<float> maxX("panel.maxX", 5,  1, 10);
    //   pangolin::Var<float> minY("panel.minY",-5,-10, -1);
    //   pangolin::Var<float> maxY("panel.maxY", 5,  1, 10);

    //num_batches = 200 will result in pushing 200 points through this training network.
    pangolin::Var<int> num_batches("panel.batches",10,1,testLabels.size()/(2*num));

    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0,1.0,1.0,1.0);


    for(int frame_num = 0 ; !pangolin::ShouldQuit() ; frame_num++) {

        if (pangolin::HasResized()) {
            pangolin::DisplayBase().ActivateScissorAndClear();
        }


        //loss->layer_param().contrastive_loss_param().margin( margin);


//        solver->params().set_base_lr(base_lr);
//        solver->params().set_momentum(moment);


        if (step || frame_num <1) {

            //step the solver
            //note: the batch size is set in the train_test.prototex file
            //this will run solver_type: SGD etc.
            if(frame_num >0) solver->Step(stepIters);

            mappedPoints.clear();
            for (int batch=0; batch<num_batches; ++batch) {
                //copy a example pair of images and set them as the input blob "pairBlob"
                //note: this pairing is based on data ordering not semantic pairing.
                memcpy(pairBlob->mutable_cpu_data(),testData + 2*num*batch*width_input_img*height_input_img,2*num*width_input_img*height_input_img*sizeof(float));

                //forwarding the pre-loaded pair above through layers
                //1 and above.

                testNet->ForwardFromTo(1, testNet->layers().size()-2);//cut out the loss.

                for (int i=0; i<num; ++i) {
                    //get the resulting feature blob for image 1
                    mappedPoints.push_back(make_float3(featABlob->cpu_data()[nD_output*i],featABlob->cpu_data()[nD_output*i+1],featABlob->cpu_data()[nD_output*i+2]));
                    //get the resulting feature blob for image 2
                    mappedPoints.push_back(make_float3(featBBlob->cpu_data()[nD_output*i],featBBlob->cpu_data()[nD_output*i+1],featBBlob->cpu_data()[nD_output*i+2]));
                }
            }
        }

        imgDisp.ActivateScissorAndClear(camState);

        glPushMatrix();
        glTranslated(0,0,0);
        //glutWireCube(1);
        glColor3f(0.6,0.6,0.6);
        pangolin::glDraw_y0(0.1,10);
        glColor3f(0.8,0.8,0.8);
        pangolin::glDraw_y0(0.2,50);
        glPopMatrix();


        //imgDisp.ActivateScissorAndClear();
        //imgDisp.ActivatePixelOrthographic();


        glPushMatrix();
        //glScalef(imgDisp.GetBounds().w/(maxX-minX),imgDisp.GetBounds().h/(maxY-minY),1);
        //glTranslatef(-minX,-minY,0);

        glColor3f(0,0,0);
        glPointSize(3);
        glBegin(GL_POINTS);

        for (int i=0; i<mappedPoints.size(); ++i) {
            //glColor3ubv((unsigned char*)&colors[testLabels[i]]);
            float4 c = glColorHSV(testLabels[i]/numSemanticClasses);
            glColor4f(c.x,c.y,c.z,1);
            glVertex3f(mappedPoints[i].x,mappedPoints[i].y,mappedPoints[i].z);
            //glVertex3f(mappedPoints[i].x,testLabels[i]/numSemanticClasses,mappedPoints[i].z);

        }

        glEnd();

        glPopMatrix();

        pangolin::FinishGlutFrame();

    }

    return 0;
}

void loadMNISTLabels(const std::string filename, std::vector<unsigned char>& labels) {

    std::ifstream stream;
    stream.open(filename,std::ios_base::in | std::ios_base::binary);

    if (!stream.good()) {
        std::cerr << "couldn't find " << filename << std::endl; return;
    }

    int32_t magicNumber, numLabels;
    stream.read((char*)&magicNumber,sizeof(int32_t));
    magicNumber = be32toh(magicNumber);

    if (magicNumber != mnistMagicLabelNumber) {
        std::cerr << "magic number did not match" << std::endl; return;
    }

    stream.read((char*)&numLabels,sizeof(int32_t));

    numLabels = be32toh(numLabels);

    labels.resize(numLabels);
    stream.read((char*)labels.data(),numLabels*sizeof(unsigned char));
    stream.close();
}

float* loadMNISTImages(const std::string filename) {

    std::ifstream stream;
    stream.open(filename,std::ios_base::in | std::ios_base::binary);

    if (!stream.good()) {
        std::cerr << "couldn't find " << filename << std::endl; return 0;
    }

    int32_t magicNumber, numImages, numRows, numCols;
    stream.read((char*)&magicNumber,sizeof(int32_t));
    magicNumber = be32toh(magicNumber);

    if (magicNumber != mnistMagicImageNumber) {
        std::cerr << "magic number did not match" << std::endl; return 0;
    }

    stream.read((char*)&numImages,sizeof(int32_t));
    numImages = be32toh(numImages);

    stream.read((char*)&numRows,sizeof(int32_t));
    numRows = be32toh(numRows);

    stream.read((char*)&numCols,sizeof(int32_t));
    numCols = be32toh(numCols);

    float* floatData = new float[numImages*numRows*numCols];
    unsigned char* ucharData = new unsigned char[numImages*numRows*numCols];
    stream.read((char*)ucharData,numImages*numRows*numCols*sizeof(unsigned char));
    stream.close();

    for (int i=0; i<numImages*numRows*numCols; ++i) {
        floatData[i] = ucharData[i]/255.f;
    }

    delete [] ucharData;
    return floatData;
}
